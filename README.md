# Практические работы по ТиМП

[1 практическая работа](https://gitlab.com/tusur_fb_timp/practices/-/wikis/1-%D0%BF%D1%80%D0%B0%D0%BA%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0)

- [1.1 отчет по практической работе №1](https://gitlab.com/Ichiro228/practices/-/blob/master/pr1/README.md)

[2 практическая работа. Однопроходные алгоритмы](https://gitlab.com/tusur_fb_timp/practices/-/wikis/2-%D0%BF%D1%80%D0%B0%D0%BA%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0.-%D0%9E%D0%B4%D0%BD%D0%BE%D0%BF%D1%80%D0%BE%D1%85%D0%BE%D0%B4%D0%BD%D1%8B%D0%B5-%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D1%8B)

- [2.1 отчет по практической работе №2](https://gitlab.com/Ichiro228/practices/-/blob/master/pr2/README.md)

[3 практическая работа. Односвязные списки](https://gitlab.com/tusur_fb_timp/practices/-/wikis/3-%D0%BF%D1%80%D0%B0%D0%BA%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0.-%D0%9E%D0%B4%D0%BD%D0%BE%D1%81%D0%B2%D1%8F%D0%B7%D0%BD%D1%8B%D0%B5-%D1%81%D0%BF%D0%B8%D1%81%D0%BA%D0%B8)

- [3.1 отчет по практической работе №3](https://gitlab.com/Ichiro228/practices/-/blob/master/pr3/README.md)

[4 практическая работа. Двусвязные списки](https://gitlab.com/tusur_fb_timp/practices/-/wikis/4-%D0%BF%D1%80%D0%B0%D0%BA%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0.-%D0%94%D0%B2%D1%83%D1%81%D0%B2%D1%8F%D0%B7%D0%BD%D1%8B%D0%B5-%D1%81%D0%BF%D0%B8%D1%81%D0%BA%D0%B8)

- [4.1 отчет по практической работе №4](https://gitlab.com/Ichiro228/practices/-/blob/master/pr4/README.md)

[5 практическая работа. Бинарные деревья поиска](https://gitlab.com/tusur_fb_timp/practices/-/wikis/5-%D0%BF%D1%80%D0%B0%D0%BA%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0.-%D0%91%D0%B8%D0%BD%D0%B0%D1%80%D0%BD%D1%8B%D0%B5-%D0%B4%D0%B5%D1%80%D0%B5%D0%B2%D1%8C%D1%8F-%D0%BF%D0%BE%D0%B8%D1%81%D0%BA%D0%B0)

- [5.1 отчет по практической работе №5](https://gitlab.com/Ichiro228/practices/-/blob/master/pr5/README.md)

[6 практическая работа. Обход бинарного дерева](https://gitlab.com/tusur_fb_timp/practices/-/wikis/6-%D0%BF%D1%80%D0%B0%D0%BA%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0.-%D0%9E%D0%B1%D1%85%D0%BE%D0%B4-%D0%B1%D0%B8%D0%BD%D0%B0%D1%80%D0%BD%D0%BE%D0%B3%D0%BE-%D0%B4%D0%B5%D1%80%D0%B5%D0%B2%D0%B0#%D0%BF%D1%80%D1%8F%D0%BC%D0%BE%D0%B9-%D0%BE%D0%B1%D1%85%D0%BE%D0%B4-%D0%B4%D0%B5%D1%80%D0%B5%D0%B2%D0%B0)

- [6.1 отчет по практической работе №6](https://gitlab.com/Ichiro228/practices/-/blob/master/pr6/README.md)

[7 практическая работа. Алгоритмы сортировки #1](https://gitlab.com/tusur_fb_timp/practices/-/wikis/7-%D0%BF%D1%80%D0%B0%D0%BA%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F-%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%B0.-%D0%90%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D1%8B-%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B8-%231)

- [7.1 отчет по практической работе №7](https://gitlab.com/Ichiro228/practices/-/blob/master/pr7/README.md) 

[8 практическая работа. Алгоритмы сортировки #1]()

- [8.1 отчет по практической работе №8](https://gitlab.com/Ichiro228/practices/-/blob/master/pr8/README.md)