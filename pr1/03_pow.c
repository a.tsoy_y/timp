#include <math.h>
#include <stdio.h>

int main(void){
    double x, y;
    (void) scanf("%lf%lf", &x, &y);
    printf("%f\n", pow(x, y));
    return(0);
}
