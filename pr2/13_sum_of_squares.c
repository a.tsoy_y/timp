#include <stdio.h>

int main(void){
    int x, y, i, k;
    long long int res;
    res=0;
    (void)scanf("%d", &x);
    for (i=0;i<x;i++) {
        (void)scanf("%d", &y);
        k=y*y;
        res=k+res;
    }
    printf("%lld\n", res);
    return(0);
}
