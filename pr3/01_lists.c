#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


typedef struct node {
	int value;          // значение, которое хранит узел 
	struct node *next;  // ссылка на следующий элемент списка
} node;

typedef struct list {
	struct node *head;  // начало списка
} list;


// инициализация пустого списка
void init(list *l){
     l->head = NULL;
}
// удалить все элементы из списка
void clean(list *l){
     l->head =NULL;
}
// проверка на пустоту списка
bool is_empty(list *l){
     if (l != NULL){return(false);}
     else{return(true);}
}

// поиск элемента по значению. вернуть NULL если эжемент не найден
node *find(list *l, int value){
     node * buf = l->head;
     while (buf->value != value){
           if ( buf->next == NULL){return(NULL);}
           buf=buf->next;
     }
     return(buf);
}

// вставка значения в конец списка, вернуть 0 если успешно
int push_back(list *l, int value){    
    node *buf2;
    buf2 = l->head;
    node *buf = (node*)malloc(sizeof(node));
    buf->value = value;
    buf->next = NULL;
    if (buf2 == NULL){l->head = buf;return(0);}
    while (buf2->next != NULL){buf2 = buf2->next;}
    buf2->next = buf;
    return(0);
}

// вставка значения в начало списка, вернуть 0 если успешно
int push_front(list *l, int value){
    node * buf = malloc(sizeof(node));
    buf->value = value;
    buf->next = l->head;
    l->head = buf;
    return(0);
}

// вставка значения после указанного узла, вернуть 0 если успешно
int insert_after(node *j, int value){
    node * buf2 = malloc(sizeof(node));
    buf2->value = value;
    buf2->next = j->next;
    j->next = buf2;
    return(0);
}

node *findj(list *l, int num){
     node* buf = l->head;
     int i;
     for (i = 1; i < num; i++){
         buf = buf->next; 
     }
     return(buf);
     
}
// удалить первый элемент из списка с указанным значением, 
// вернуть 0 если успешно
int remove_node(list *l, int value){
    node *buf1 = l->head;
    node* buf2;
    
    while (buf1->value != value)
    {
          buf2 = buf1;
          buf1 = buf1->next;
		  if(buf1->next == NULL){return(1);}
    }
    if (buf1 == l->head) l->head = buf1->next;
    else buf2->next = buf1->next;
    free(buf1);
    return(0);    
    
}

// вывести все значения из списка в прямом порядке через пробел,
// после окончания вывода перейти на новую строку
void print(list *l){
     node * buf = l->head;
     while (buf != NULL){
 	       printf("%d ", buf->value);
 	       buf = buf->next;
     }
     printf("\n");
}     

int main() {
    int n, k1, k2 ,k3, a, m, t, j, x, z;
    node *c;
    list ls;
    init(&ls);//1
    scanf("%d", &n);//2
    for (int i=0;i<n;i++){
        scanf("%d", &a);
        push_back(&ls, a);
    }
    print(&ls);//3
    
    scanf("%d", &k1);//4
    if (find(&ls, k1) == NULL){printf("0 ");}
    else{printf("1 ");}
    
    scanf("%d", &k2);
    if (find(&ls, k2) == NULL){printf("0 ");}
    else{printf("1 ");}
    
    scanf("%d", &k3);
    if (find(&ls, k3) == NULL){printf("0\n");}
    else{printf("1\n");}//5
    
    scanf("%d", &m);//6
    push_back(&ls, m);
    print(&ls);//7
    
    scanf("%d", &t);//8
    push_front(&ls, t);//9
    print(&ls);//10
    
    scanf("%d", &j);//11
    scanf("%d", &x);
    if (j != 0){
       c = findj(&ls, j);
       insert_after(c, x);
    }
    else{
         push_front(&ls, x);
    }
    print(&ls);
    
    scanf("%d", &z);
    remove_node(&ls, z);
    print(&ls);
    clean(&ls);
    return 0;
};