#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node{
    struct node *next;
    struct node *prev;
    int value;
} node;

typedef struct list{
    struct node *head;
    struct node *tail;
    int num;
} list;


void init(list *l){
     l->tail = NULL;
     l->head = NULL;
     l->num = 0;
};

bool is_empty(list *l){
     if (l->num == 0){return(true);}
     else{return false;}
};

void clean(list *l){
     node *buf;
     node *buf1;
     buf = l->head;
     while (buf != NULL){
           buf1 = buf->next;
           free(buf);
           buf = buf1;
     } 
};



int push_first(list *l, int value){
    node *buf = malloc(sizeof(node));
    buf->prev = NULL;
    buf->next = NULL;
    buf->value = value;
    l->num = 1;
    l->tail = buf;
    l->head = buf;
    return(0);
};

node *find(list *l, int value) {
	  node *buf = l->head;
	  while (buf != NULL){
		if (buf->value == value){return buf;}
		buf = buf->next;
	 }
	 return(NULL);
};

int push_back(list *l, int value){
    if (is_empty(l)){push_first(l, value);}
    else{
        node *buf = malloc(sizeof(node));
        buf->value = value;
        buf->next = NULL;
        buf->prev = l->tail;
        l->tail->next = buf;
        l->tail = buf;
        l->num = l->num + 1;
    }
    return(0);
};

int push_front(list *l, int value){
    if (is_empty(l)){push_first(l, value);}   
    else{
        node *buf;
	buf = malloc(sizeof(node));
        buf->prev = NULL;
        buf->next = l->head;
        buf->value = value;
        l->head->prev = buf;
        l->head = buf;
        l->num = l->num + 1;
    }
    return(0);
};

int insert_after(list *l, int n, int value){
    if (l->num == 0){return(1);} 
    else if (n > l->num || n<1){return(1);} 
    else if (n == l->num){return(push_back(l, value));}
    node *buf;
    node *buf1;
    buf1 = malloc(sizeof(node));
    buf1->value = value;
    if (n > l->num/2){
       buf = l->tail;
       for (int i=l->num; i>n; i = i-1)
           buf = buf->prev;
    }
    else {
         buf = l->head;
         for (int i=1; i<n; i++) 
             buf = buf->next;
    }
    l->num = l->num + 1;
    buf1->prev = buf;
    buf1->next = buf->next;
    buf->next->prev = buf1;
    buf->next = buf1;
    return(0);
};

int insert_before(list *l, int n, int value) {
    if (l->num == 0){return(1);}
    else if (n > l->num || n<1){return(1);}
    else if (n == 1){return(push_front(l, value));}
    node *buf;
    node *buf1;
    buf1 = malloc(sizeof(node));
    buf1->value = value;
    if (n > l->num/2) {
       buf = l->tail;
       for (int i = l->num; i > n-1; i = i-1)
           buf = buf->prev;
    }
    else {
         buf = l->head;
         for (int i=1; i<n-1; i++) 
             buf = buf->next;
    }
    l->num = l->num+1;
    buf1->prev = buf;
    buf1->next = buf->next;
    buf->next->prev = buf1;
    buf->next = buf1;
    return(0);
};

int remove_first(list *l, int value) {
    if (is_empty(l)){return(1);}
    else {
         node *buf;
         buf = l->head;
         while (buf != NULL) {
               if (buf->value == value) {
                  if (buf == l->head) {
                     l->head = buf->next;
                     l->head->prev = NULL;
                  }
                  else if (buf == l->tail) {
                       l->tail->next = NULL;
                       l->tail = buf->prev;
                  }
                  else {
                       buf->prev->next = buf->next;
                       buf->next->prev = buf->prev;
                  }
               l->num = l->num-1;
               free(buf);
               return(0);
               }
               buf = buf->next;
         }
    return(1);
    }
};

int remove_last(list *l, int value) {
    if (is_empty(l)){return(1);}
    else {    
         node *buf;
         buf = l->tail;
         while (buf != NULL) {
               if (buf->value == value) {
                  if (buf == l->head) {
                     l->head->prev = NULL;
                     l->head = buf->next;
                  }
                  else if (buf == l->tail) {
                          l->tail->next = NULL;
                          l->tail = buf->prev;
                  }
                  else {
                       buf->prev->next = buf->next;
                       buf->next->prev = buf->prev;
                  }
                  free(buf);
                  l->num = l->num-1;
                  return(0);
               }
               buf = buf->prev;
         }
    return(1);
    }
};

void print(list *l) {
     node *buf;
     buf = l->head;
     for (int i=0; i < l->num; i++){
	 printf("%d ", buf->value);
	 buf = buf->next;
     }
     printf("\n");
};

void print_invers(list *l) {
     node *buf;
     buf = l->tail;
     for (int i = 0; i < l->num; i++){
	 printf("%d ", buf->value);
	 buf = buf->prev;
     }
     printf("\n");
};

int main(){
    list ls;
    int n, a, k1, k2, k3, m, t, j, x, u, y, z, r;
    scanf("%d", &n);
    init(& ls);
    for (int i = 0; i < n; i++){
	scanf("%d", &a);
	push_back(&ls, a);
    }
    print(&ls);
    scanf("%d", &k1);
    if (find(&ls, k1) == NULL){printf("0 ");}
    else{printf("1 ");}
    scanf("%d", &k2);
    if (find(&ls, k2) == NULL){printf("0 ");}
    else{printf("1 ");}
    scanf("%d", &k3);
    if (find(&ls, k3) == NULL){printf("0\n");}
    else{printf("1\n");}
    printf("\n");
    scanf("%d", &m);
    push_back(&ls, m);
    print_invers(&ls);
    scanf("%d", &t);
    push_front(&ls, t);
    print(&ls);
    scanf("%d", &j);
    scanf("%d", &x);
    insert_after(&ls, j, x);
    print_invers(&ls);
    scanf("%d", &u);
    scanf("%d", &y);
    insert_before(&ls, u, y);
    print(&ls);
    scanf("%d", &z);
    remove_first(&ls, z);
    print_invers(&ls);
    scanf("%d", &r);
    remove_last(&ls, r);
    print(&ls);
    clean(&ls);
    return 0;
}