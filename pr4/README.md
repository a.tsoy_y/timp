# Программа "Двусвязные списки"
### Задание
Реализовать программу, которая:
1. считывает количество элементов n, 0 < n <= 2147483647;
1. создает пустой список, считывает n элементов a, |a| <= 2147483647 и заносит их в список;
1. выводит содержимое списка, используя функцию print;
1. считывает k1, k2, k3 (|k| <= 2147483647) и выводит "1", если в списке существует элемент с таким значением и "0" если нет (выводить через пробел, например "1 0 1");
1. считывает m, |m| <= 2147483647 и вставляет его в конец списка;
1. выводит содержимое списка, используя функцию print_invers;
1. считывает t, |t| <= 2147483647 и вставляет его в начало списка;
1. выводит содержимое списка, используя функцию print;
1. считывает j и x (0 < j <= 2147483647, |x| <= 2147483647) и вставляет значение x после j-ого элемента списка;
1. выводит содержимое списка, используя функцию print_invers;
1. считывает u и y (0 < u <= 2147483647, |y| <= 2147483647) и вставляет значение y перед u-ым элементом списка;
1. выводит содержимое списка, используя функцию print;
1. считывает z, |z| <= 2147483647 и удаляет первый элемент (при его наличии), хранящий значение z из списка;
1. выводит содержимое списка, используя функцию print_invers;
1. считывает r, |r| <= 2147483647 и удаляет последний элемент (при его наличии), хранящий значение r из списка;
1. выводит содержимое списка, используя функцию print;
1. очищает список.
### Ход работы
 Для выполнения задания практической работы нужно было реализовать функции для работы с двусвязными списками.
В ходе выполнения работы были реализованны функции создания пустого списка, удаления всех элементов списка, добавления нового элемента в начало или конец списка, поиска элемента списка по значению, вставка значения после указанного узла, удаления первого элемента с указанным значением и вывода списка.
Каждый элемент односвязного списка имеет какое-то значение и адрес на следующий элемент (в последнем элементе хранится адрес = NULL). 
int num - текущий номер элемента списка.
int value - значение, которое хранится в элементе списка
Основные команды которые использовались при написании программы:
* typedef struct name - создание списка
* l->head - ссылка на начало списка
* l->tail - ссылка на конец списка
* name->next - ссылка на следующий элемент
* name->prev - ссылка на предыдущий элемент
* free(name) - очистка элемента 
* malloc(sizeof(name)) - выделение памяти
 
Код программы:
```
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct node{
    struct node *next;
    struct node *prev;
    int value;
} node;

typedef struct list{
    struct node *head;
    struct node *tail;
    int num;
} list;


void init(list *l){
     l->tail = NULL;
     l->head = NULL;
     l->num = 0;
};

bool is_empty(list *l){
     if (l->num == 0){return(true);}
     else{return false;}
};

void clean(list *l){
     node *buf;
     node *buf1;
     buf = l->head;
     while (buf != NULL){
           buf1 = buf->next;
           free(buf);
           buf = buf1;
     } 
};



int push_first(list *l, int value){
    node *buf = malloc(sizeof(node));
    buf->prev = NULL;
    buf->next = NULL;
    buf->value = value;
    l->num = 1;
    l->tail = buf;
    l->head = buf;
    return(0);
};

node *find(list *l, int value) {
	  node *buf = l->head;
	  while (buf != NULL){
		if (buf->value == value){return buf;}
		buf = buf->next;
	 }
	 return(NULL);
};

int push_back(list *l, int value){
    if (is_empty(l)){push_first(l, value);}
    else{
        node *buf = malloc(sizeof(node));
        buf->value = value;
        buf->next = NULL;
        buf->prev = l->tail;
        l->tail->next = buf;
        l->tail = buf;
        l->num = l->num + 1;
    }
    return(0);
};

int push_front(list *l, int value){
    if (is_empty(l)){push_first(l, value);}   
    else{
        node *buf;
	buf = malloc(sizeof(node));
        buf->prev = NULL;
        buf->next = l->head;
        buf->value = value;
        l->head->prev = buf;
        l->head = buf;
        l->num = l->num + 1;
    }
    return(0);
};

int insert_after(list *l, int n, int value){
    if (l->num == 0){return(1);} 
    else if (n > l->num || n<1){return(1);} 
    else if (n == l->num){return(push_back(l, value));}
    node *buf;
    node *buf1;
    buf1 = malloc(sizeof(node));
    buf1->value = value;
    if (n > l->num/2){
       buf = l->tail;
       for (int i=l->num; i>n; i = i-1)
           buf = buf->prev;
    }
    else {
         buf = l->head;
         for (int i=1; i<n; i++) 
             buf = buf->next;
    }
    l->num = l->num + 1;
    buf1->prev = buf;
    buf1->next = buf->next;
    buf->next->prev = buf1;
    buf->next = buf1;
    return(0);
};

int insert_before(list *l, int n, int value) {
    if (l->num == 0){return(1);}
    else if (n > l->num || n<1){return(1);}
    else if (n == 1){return(push_front(l, value));}
    node *buf;
    node *buf1;
    buf1 = malloc(sizeof(node));
    buf1->value = value;
    if (n > l->num/2) {
       buf = l->tail;
       for (int i = l->num; i > n-1; i = i-1)
           buf = buf->prev;
    }
    else {
         buf = l->head;
         for (int i=1; i<n-1; i++) 
             buf = buf->next;
    }
    l->num = l->num+1;
    buf1->prev = buf;
    buf1->next = buf->next;
    buf->next->prev = buf1;
    buf->next = buf1;
    return(0);
};

int remove_first(list *l, int value) {
    if (is_empty(l)){return(1);}
    else {
         node *buf;
         buf = l->head;
         while (buf != NULL) {
               if (buf->value == value) {
                  if (buf == l->head) {
                     l->head = buf->next;
                     l->head->prev = NULL;
                  }
                  else if (buf == l->tail) {
                       l->tail->next = NULL;
                       l->tail = buf->prev;
                  }
                  else {
                       buf->prev->next = buf->next;
                       buf->next->prev = buf->prev;
                  }
               l->num = l->num-1;
               free(buf);
               return(0);
               }
               buf = buf->next;
         }
    return(1);
    }
};

int remove_last(list *l, int value) {
    if (is_empty(l)){return(1);}
    else {    
         node *buf;
         buf = l->tail;
         while (buf != NULL) {
               if (buf->value == value) {
                  if (buf == l->head) {
                     l->head->prev = NULL;
                     l->head = buf->next;
                  }
                  else if (buf == l->tail) {
                          l->tail->next = NULL;
                          l->tail = buf->prev;
                  }
                  else {
                       buf->prev->next = buf->next;
                       buf->next->prev = buf->prev;
                  }
                  free(buf);
                  l->num = l->num-1;
                  return(0);
               }
               buf = buf->prev;
         }
    return(1);
    }
};

void print(list *l) {
     node *buf;
     buf = l->head;
     for (int i=0; i < l->num; i++){
	 printf("%d ", buf->value);
	 buf = buf->next;
     }
     printf("\n");
};

void print_invers(list *l) {
     node *buf;
     buf = l->tail;
     for (int i = 0; i < l->num; i++){
	 printf("%d ", buf->value);
	 buf = buf->prev;
     }
     printf("\n");
};

int main(){
    list ls;
    int n, a, k1, k2, k3, m, t, j, x, u, y, z, r;
    scanf("%d", &n);
    init(& ls);
    for (int i = 0; i < n; i++){
	scanf("%d", &a);
	push_back(&ls, a);
    }
    print(&ls);
    scanf("%d", &k1);
    if (find(&ls, k1) == NULL){printf("0 ");}
    else{printf("1 ");}
    scanf("%d", &k2);
    if (find(&ls, k2) == NULL){printf("0 ");}
    else{printf("1 ");}
    scanf("%d", &k3);
    if (find(&ls, k3) == NULL){printf("0\n");}
    else{printf("1\n");}
    printf("\n");
    scanf("%d", &m);
    push_back(&ls, m);
    print_invers(&ls);
    scanf("%d", &t);
    push_front(&ls, t);
    print(&ls);
    scanf("%d", &j);
    scanf("%d", &x);
    insert_after(&ls, j, x);
    print_invers(&ls);
    scanf("%d", &u);
    scanf("%d", &y);
    insert_before(&ls, u, y);
    print(&ls);
    scanf("%d", &z);
    remove_first(&ls, z);
    print_invers(&ls);
    scanf("%d", &r);
    remove_last(&ls, r);
    print(&ls);
    clean(&ls);
    return 0;
}
 ```

 ### Результаты
Пример прохождения тестов и работы программы:

![](https://i.imgur.com/1G8hFdr.jpg)

Программа проходит все тесты, а следовательно работает правильно.
[Ссылка на программу](https://gitlab.com/Ichiro228/practices/-/blob/master/pr4/01_lists.c)
 

# Таблица прохождения тестов
Название  | Py test
-|-
easy|✓
middle|✓
hard|✓