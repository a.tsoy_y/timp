# Программа "Бинарные деревья поиска"
### Задание
Реализовать программу, которая:
1. создает пустое дерево, считывает 4 элемента ai, |ai| <= 2147483647 и заносит их в дерево;
1. выводит дерево (используя функцию print_tree) и пустую строку;
2. считывает 3 элемента ai, |ai| <= 2147483647 и заносит их в дерево;
1. выводит дерево и пустую строку;
1. считывает m1, |m1| <= 2147483647 и ищет элемент с заданным значением в дереве;
выводит через пробел значение предка и потомков найденного элемента,
если нет значений предка или потомков вывести "_" вместо таких значений;
вывести "-", если элемент не найден; вывести пустую строку;
1. считывает m2, |m2| <= 2147483647 и ищет элемент с заданным значением в дереве;
выводит через пробел значение предка и потомков найденного элемента,
если нет значений предка или потомков вывести "_" вместо таких значений;
вывести "-", если элемент не найден; вывести пустую строку;
1. считывает m3, |m3| <= 2147483647 и удаляет из дерева элемент с заданным значением (если такой элемент есть);
1. выводит дерево и пустую строку;
2. выполняет левый поворот дерева относительно корня, пока это возможно;
1. выводит дерево и пустую строку;
2. выполняет правый поворот дерева относительно корня, пока это возможно;
1.выводит дерево и пустую строку;
1. выводит на экран количество элементов в дереве и пустую строку
1. очищает дерево
1. выводит дерево и пустую строку;

### Ход работы
 Для выполнения задания практической работы нужно было реализовать функции для работы с бинарными деревьями поиска.
В ходе выполнения работы были реализованны функции создания пустого дерева, удаления всех элементов дерева, вставка значения в дерево, удаление минимального элемента из поддерева, правое и левое вращения поддерева, вывод значений дерева.
Каждый элемент дерева имеет какое-то значение и адрес на два следующих элемента (если у элемента нету детей в нем хранится адрес = NULL). 
Код программы:
```
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>
typedef struct node
{
    int data;
    struct node *left;
    struct node *right;
    struct node *parent;
} node;
typedef struct tree
{ 
  struct node *head;
} tree;
void init(tree *t)
{
    t->head = NULL;
}
node* clNode(node* n)
{
    if(n!=NULL)
    {
        clNode(n->left);
        clNode(n->right);
        if(n->parent!=NULL)
            n->parent = NULL;
        if(n->left!=NULL)
            n->left = NULL;
        if(n->right!=NULL)
            n->right = NULL;
        free(n);
    }
    return NULL;
}
void clean(tree *t)
{
    clNode( t->head );
    t->head = NULL;
}
bool is_empty(tree *t)
{
    return t->head == NULL;
}
node *find(tree *t, int value)
{
    if(!is_empty(t))
    {
        node *root = t->head;
        while(root) 
        {
            if (root->data > value) {
                root = root->left;
                continue;
            } else if (root->data < value) {
                root = root->right;
                continue;
            } else {
                return root;
            }
        }
        return NULL;
    } else return NULL;
}
node *findNode(node *root, int value)
{
    if(root)
    {
        while(root) 
        {
            if (root->data > value) {
                root = root->left;
                continue;
            } else if (root->data < value) {
                root = root->right;
                continue;
            } else {
                return root;
            }
        }
        return NULL;
    } else return NULL;
}
node* getNode( int value, node *parent) {
    node* tmp = (node*) malloc(sizeof(node));
    tmp->left = tmp->right = NULL;
    tmp->data = value;
    tmp->parent = parent;
    return tmp;
}
int insert(tree* t, int value)
{
    if( is_empty(t))
    {
        t->head = getNode(value, NULL);
        return 0;
    }
    if( find(t, value) != NULL)
        return 1;
    node *tmp = t->head;
    while(tmp != NULL)
    {
        if (value > tmp->data) 
        {
            if (tmp->right) 
            {
                tmp = tmp->right;
                continue;
            } 
            else 
            {
                tmp->right = getNode(value, tmp);
                return 0;
            }
        } 
        else if (value < tmp->data) 
        {
            if (tmp->left) 
            {
                tmp = tmp->left;
                continue;
            } else 
            {
                tmp->left = getNode(value, tmp);
                return 0;
            }
        } else 
        {
            return 2;
        }
    }
    return 2;
}
void insertNode(node* n, int value)
{
    if( findNode(n, value) != NULL)
        return;
    node *tmp = n;
    while(tmp != NULL)
    {
        if (value > tmp->data) 
        {
            if (tmp->right) 
            {
                tmp = tmp->right;
                continue;
            } 
            else 
            {
                tmp->right = getNode(value, tmp);
                return;
            }
        } 
        else if (value < tmp->data) 
        {
            if (tmp->left) 
            {
                tmp = tmp->left;
                continue;
            } else 
            {
                tmp->left = getNode(value, tmp);
                return;
            }
        } else 
        {
            return;
        }
    }
}
node* getMinNode(node *root) 
{
    while (root->left) {
        root = root->left;
    }
    return root;
}
node* getMaxNode(node *root) 
{
    while (root->right) {
        root = root->right;
    }
    return root;
}
void removeNodeByPtr(node *aim) 
{
    if (aim->left && aim->right) {
        node *localMax = aim->left->data > aim->right->data ? aim->left : aim->right; aim->data = localMax->data;
        if(localMax->left)
        {
            if(aim->left)
            {
                insertNode( aim->left, localMax->left->data);
                removeNodeByPtr(localMax->left);
            } else 
            { 
                int value = localMax->left->data;
                removeNodeByPtr(localMax->left);
                insertNode(aim->left , value);
            }
        }
        removeNodeByPtr(localMax);
        return;
    } else if (aim->left) {
        if (aim == aim->parent->left) {
            aim->parent->left = aim->left;
        } else {
            aim->parent->right = aim->left;
        }
    } else if (aim->right) {
        if (aim == aim->parent->right) {
            aim->parent->right = aim->right;
        } else {
            aim->parent->left = aim->right;
        }
    } else {
        if (aim == aim->parent->left) {
            aim->parent->left = NULL;
        } else {
            aim->parent->right = NULL;
        }
    }
    free(aim);
}
int remove_node(tree* t, int value)
{
    node *n = find(t, value);
    if(n != NULL)
    {
        if( nodeCount(t->head) > 1)
            removeNodeByPtr(n);
        else clean(t);
        return 0;
    } else return 1;
}
int remove_min(node* n)
{
    node *r = getMinNode(n);
    int res = r->data;
    removeNodeByPtr(r);
    return res;
}
void lTor(node *root) {
    node *C = NULL, *a = NULL, *b = NULL;
    a = root;
    b = a->right;
    if (b == NULL) {
        return ;
    }
    C = b->left;
    b->parent = a->parent;
    if (a->parent != NULL) {
        if (a->parent->data > a->data)
                a->parent->left = b;
                     else
                             a->parent->right = b;
    }
    if (C != NULL)
        C->parent = a;
    a->right = C;
    a->parent = b;
    b->left = a;
}
void rTol(node *root) {
    node *C = NULL, *a = NULL, *b = NULL;
    b = root;
    a = b->left;
    if (a == NULL) {
        return;
    }
    C = a->right;
    a->parent = b->parent;
    if (b->parent != NULL) {
        if (b->parent->data > b->data)
              b->parent->left = a;
                    else
                         b->parent->right = a;
    }
    if (C != NULL)
          C->parent = b;
    b->left = C;
    b->parent = a;
    a->right = b; 
}
int rotate_right(node* n)
{
    if(n != NULL)
    {
        if( n->right != NULL )
        {
            lTor(n);
            return 0;
        } else return 1;
    } else return 1;
}
int rotate_left(node* n)
{
    if(n != NULL)
    {
        if(n->left != NULL)
        {
            rTol(n);
            
            return 0;
        } else return 1;
    } else return 1;
}
void print_node(node* n)
{
    if(n != NULL )
    {
        int par = n->parent != NULL ? n->parent->data : -1;
        int lef = n->left != NULL ? n->left->data : -1;
        int rig = n->right != NULL ? n->right->data : -1;
        printf( ( par != -1 ? "%d " : "_ " ) , par);
        printf( ( lef != -1 ? "%d " : "_ " ) , lef);
        printf( ( rig != -1 ? "%d" : "_" ), rig);
        printf( "\n\n" );
    } else printf("-\n\n");
}
int _powf( int x, int y )
{
    int tmp = x;
    while(y > 1)
    {
        y--;
        tmp = tmp*x; 
    }
    return tmp;
}
typedef struct struct_item {
    void* payload;
    struct struct_item* next;
} item;
typedef struct struct_q {
    item* begin;
    item* end;
}q;
q* create_q() {
    q* res = malloc(sizeof(q));
    res->begin = NULL;
    res->end = NULL;
    return res;
}
void q_push(q* q, void* payload) {
    item* i = malloc(sizeof(item));
    i->next = NULL;
    i->payload = payload;
    if (q->end == NULL) {
        q->begin = i;
        q->end = i;
    }
    else {
        q->end->next = i;
        q->end = i;
    }
}
void* q_pop(q* q) {
    if (q->begin == NULL) return NULL;
    item* current = q->begin;
    q->begin = q->begin->next;
    if (q->begin == NULL) q->end = NULL;
    void* payload = current->payload;
    free(current);
    return payload;
}
void print(node* root) {
    if(root == NULL)
    {
        printf("-\n\n");
        return;
    }
    q* q_current = NULL;
    q* q_next = create_q();
    q_push(q_next, (void*)root);
    int isNotLastLevel;
    do {
        free(q_current);
        q_current = q_next;
        q_next = create_q();
        void* payload;
        isNotLastLevel = 0;
        while (q_current->begin != NULL) {
            payload = q_pop(q_current);
            if (payload != NULL) {
                node* n = (node*)payload;
                printf("%d ", n->data);
                q_push(q_next, n->left);
                q_push(q_next, n->right);
                isNotLastLevel = isNotLastLevel || n->left || n->right;
            }
            else {
                printf("_ ");
                q_push(q_next, NULL);
                q_push(q_next, NULL);
            }
        }
        puts("");
    } while (isNotLastLevel);
    printf("\n");
}
void print_tree(tree* t)
{
    print(t->head);
}
int nodeCount(node* n)
{
    if(!n) return 0;
    if (n->left == NULL && n->right == NULL)
        return 1;
    int left, right;
    if (n->left != NULL)
        left = nodeCount(n->left);
    else
        left = 0;
    if (n->right != NULL)
        right = nodeCount(n->right); 
    else 
        right = 0;
    return left+right+1;
}
int main(void)
{
    int i,a;
    tree *t = malloc(sizeof(tree));
    init(t);
    for (i = 0; i < 4; i++)
    {
        scanf("%d", &a);
        insert(t, a);
    }
    print( t->head );
    for (i = 0; i < 3; i++)
    {
        scanf("%d", &a);
        insert(t, a);
    }
    print(t->head);
    int m1,m2,m3;
    scanf("%d", &m1);
    print_node(find(t, m1));
    scanf("%d", &m2);
    print_node(find( t, m2));
    scanf("%d", &m3);
    remove_node(t, m3);
    print(t->head);
    while(rotate_right(t->head) != 1)
            t->head = t->head->parent;
    print(t->head);
    while(rotate_left(t->head) != 1)
        t->head = t->head->parent;
    print(t->head);
    printf("%d\n\n", nodeCount(t->head));
    clean(t);
    print(t->head);
    return 0;
}
 ```

 ### Результаты
Пример прохождения тестов и работы программы:

![](https://i.imgur.com/BnOnA9d.jpg)

Тест 0

![](https://i.imgur.com/EmWxLKj.jpg)

Тест 1

Программа проходит все тесты, а следовательно работает правильно.
[Ссылка на программу](https://gitlab.com/Ichiro228/practices/-/blob/master/pr5/01_bst.c)
 

# Таблица прохождения тестов
Название  | Py test
-|-
тест 0|✓
тест 1|✓