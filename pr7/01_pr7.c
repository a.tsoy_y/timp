#include<stdio.h>
#include<malloc.h>

void qsort(int *arr, int begin, int end, int len)
{
    FILE *S1;
    S1 = fopen("quicksort.log", "a");
    int left = begin, right = end, base;
    base = arr[(left + right)/2];
    do
    {
////
    		for (int i = 0; i<len; i++){
			fprintf(S1, "%d ", arr[i]);
   		 }
   		 fprintf(S1, "\n");
////
        while(arr[left] < base) left++;
        while(arr[right] > base) right--;
        if(left <= right)
        {
            int tmp = arr[left];
            arr[left] = arr[right];
            arr[right] = tmp;
            left++;
            right--;

        }
    } while(left <= right);

    if ( begin < right )
        qsort(arr, begin, right, len);
    if ( end > left )
        qsort(arr, left, end, len);
}
void sorting_function(int *arr, int arr_len)
{
    int len = arr_len;
    qsort(arr, 0, arr_len-1, len);
}


void sift(int *arr1, int root, int bottom, int len)
{
  FILE *S2;
  S2 = fopen("heapsort.log", "a");
  int maxChild, done = 0;
////
    		for (int i = 0; i<len; i++){
			fprintf(S2, "%d ", arr1[i]);
   		 }
   		 fprintf(S2, "\n");
////
  while((root * 2 + 1 <= bottom) && (!done))
  {
    if(root * 2 + 2 <= bottom)
    {
        if(arr1[root * 2 + 2] > arr1[root * 2 + 1])
            maxChild = root * 2 + 2;
        else
            maxChild = root * 2 + 1;

    }
    else
        maxChild = root * 2 + 1;
    if(arr1[root] < arr1[maxChild])
    {
      int temp = arr1[root];
      arr1[root] = arr1[maxChild];
      arr1[maxChild] = temp;
      root = maxChild;
    }
    else
      done = 1;
  }
////
    		for (int i = 0; i<len; i++){
			fprintf(S2, "%d ", arr1[i]);
   		 }
   		 fprintf(S2, "\n");
////
}

void sorting_function1(int *arr1, int arr_len)
{
  int len = arr_len; 
  for (int i = (arr_len / 2) - 1; i >= 0; i--)
    sift(arr1, i, arr_len - 1, len);
  for (int i = arr_len - 1; i >= 1; i--)
  {
    int temp = arr1[0];
    arr1[0] = arr1[i];
    arr1[i] = temp;
    sift(arr1, 0, i - 1, len);
  }
}


int main() {

    int arr_len;
    scanf("%d",&arr_len);
    int *arr = (int*)malloc(sizeof(int) * arr_len);
    int *arr1 = (int*)malloc(sizeof(int) * arr_len);
    for(int i = 0; i < arr_len; i++)
    {
        scanf("%d",&arr[i]);
    }

    sorting_function(arr, arr_len);
    for(int i = 0; i < arr_len; i++)
    {
        printf("%d", arr[i]);
        if(i<arr_len-1)
        printf(" ");
    }
    printf("\n");
    sorting_function1(arr1, arr_len);
    for(int i = 0; i < arr_len; i++)
    {
        printf("%d", arr1[i]);
        if(i<arr_len-1)
        printf(" ");
    }
    printf("\n");
}
