# Программа "Обход бинарного дерева"
### Задание
Написать программу, реализующую следующие алгоритмы сортировки:

1. быстрая сортировка
1. пирамидальная сортировка



### Ход работы

##### 1 Быстрая сортировка

QuickSort является существенно улучшенным вариантом алгоритма сортировки с помощью прямого обмена, известного в том числе своей низкой эффективностью. Принципиальное отличие состоит в том, что в первую очередь производятся перестановки на наибольшем возможном расстоянии и после каждого прохода элементы делятся на две независимые группы.
Данные записанные в файл quicksort.log в ходе сортировки:

![](https://i.imgur.com/jhz2RdZ.jpg)

##### 2 Пирамидальная сортировка

Пирамидальная сортировка  — алгоритм сортировки, работающий по формуле n log n операций при сортировке n элементов. Количество применяемой служебной памяти не зависит от размера массива. Может рассматриваться как усовершенствованная сортировка пузырьком, в которой элемент всплывает / тонет по многим путям.
Данные записанные в файл heapsort.log в ходе сортировки:

![](https://i.imgur.com/JcisqSW.jpg)

Код программы:
```
#include<stdio.h>
#include<malloc.h>

void qsort(int *arr, int begin, int end, int len)
{
    FILE *S1;
    S1 = fopen("quicksort.log", "a");
    int left = begin, right = end, base;
    base = arr[(left + right)/2];
    do
    {
////
    		for (int i = 0; i<len; i++){
			fprintf(S1, "%d ", arr[i]);
   		 }
   		 fprintf(S1, "\n");
////
        while(arr[left] < base) left++;
        while(arr[right] > base) right--;
        if(left <= right)
        {
            int tmp = arr[left];
            arr[left] = arr[right];
            arr[right] = tmp;
            left++;
            right--;

        }
    } while(left <= right);

    if ( begin < right )
        qsort(arr, begin, right, len);
    if ( end > left )
        qsort(arr, left, end, len);
}
void sorting_function(int *arr, int arr_len)
{
    int len = arr_len;
    qsort(arr, 0, arr_len-1, len);
}


void sift(int *arr, int root, int bottom, int len)
{
  FILE *S2;
  S2 = fopen("heapsort.log", "a");
  int maxChild, done = 0;
////
    		for (int i = 0; i<len; i++){
			fprintf(S2, "%d ", arr[i]);
   		 }
   		 fprintf(S2, "\n");
////
  while((root * 2 + 1 <= bottom) && (!done))
  {
    if(root * 2 + 2 <= bottom)
    {
        if(arr[root * 2 + 2] > arr[root * 2 + 1])
            maxChild = root * 2 + 2;
        else
            maxChild = root * 2 + 1;

    }
    else
        maxChild = root * 2 + 1;
    if(arr[root] < arr[maxChild])
    {
      int temp = arr[root];
      arr[root] = arr[maxChild];
      arr[maxChild] = temp;
      root = maxChild;
    }
    else
      done = 1;
  }
////
    		for (int i = 0; i<len; i++){
			fprintf(S2, "%d ", arr[i]);
   		 }
   		 fprintf(S2, "\n");
////
}

void sorting_function1(int *arr, int arr_len)
{
  int len = arr_len; 
  for (int i = (arr_len / 2) - 1; i >= 0; i--)
    sift(arr, i, arr_len - 1, len);
  for (int i = arr_len - 1; i >= 1; i--)
  {
    int temp = arr[0];
    arr[0] = arr[i];
    arr[i] = temp;
    sift(arr, 0, i - 1, len);
  }
}


int main() {

    int arr_len;
    scanf("%d",&arr_len);
    int *arr = (int*)malloc(sizeof(int) * arr_len);
    for(int i = 0; i < arr_len; i++)
    {
        scanf("%d",&arr[i]);
    }

    sorting_function(arr, arr_len);
    for(int i = 0; i < arr_len; i++)
    {
        printf("%d", arr[i]);
        if(i<arr_len-1)
        printf(" ");
    }
    printf("\n");
    sorting_function1(arr, arr_len);
    for(int i = 0; i < arr_len; i++)
    {
        printf("%d", arr[i]);
        if(i<arr_len-1)
        printf(" ");
    }
    free(arr);
    printf("\n");
}
```

 ### Результаты
Пример работы программы:

![](https://i.imgur.com/nbRmWh2.jpg)


### [Ссылка на программу ](https://gitlab.com/Ichiro228/practices/-/blob/master/pr7/01_pr7.c)
 

### Таблица прохождения тестов

Название  | Успешность выполнения
-|-
быстрая |✓
пирамидаидальная |✓